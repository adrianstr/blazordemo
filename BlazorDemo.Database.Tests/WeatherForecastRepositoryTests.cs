﻿using BlazorDemo.Core.WeatherForecasts;
using BlazorDemo.Core.WeatherForecasts.ValueObjects;
using BlazorDemo.Database.Tests._Infrastructure;
using FluentAssertions;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace BlazorDemo.Database.Tests {
    [TestFixture]
    public class WeatherForecastRepositoryTests : PersistedTestBase {
        private WeatherForecastRepository _sut;

        public override void SetUp() {
            base.SetUp();
            _sut = new WeatherForecastRepository(ActDbContext);
        }

        [Test]
        public void Test_GetByDate() {
            WeatherForecast forecast1 = CreateWeatherForecast(new DateTime(2020, 3, 1));
            WeatherForecast forecast2 = CreateWeatherForecast(new DateTime(2020, 3, 2));
            WeatherForecast forecast3 = CreateWeatherForecast(new DateTime(2020, 3, 2));
            WeatherForecast forecast4 = CreateWeatherForecast(new DateTime(2020, 3, 3));

            IList<WeatherForecast> result = _sut.GetByDate(new DateTime(2020, 3, 2));

            result.Should().BeEquivalentTo(forecast2, forecast3);
        }

        private WeatherForecast CreateWeatherForecast(DateTime date) {
            var forecast = new WeatherForecast(date, new Temperature(20, TemperatureUnit.Celcius), "warm");
            ArrangeDbContext.WeatherForecasts.Add(forecast);
            ArrangeDbContext.SaveChanges();

            return forecast;
        }
    }
}