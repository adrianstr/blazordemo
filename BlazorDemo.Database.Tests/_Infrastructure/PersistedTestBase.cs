﻿using BlazorDemo.Database.EFCore;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace BlazorDemo.Database.Tests._Infrastructure {
    public class PersistedTestBase {
        [SetUp]
        public virtual void SetUp() {
            _connection = new SqliteConnection("Filename=:memory:");
            _connection.Open();

            DbContextOptions<ApplicationDbContext> dbContextOptions = new DbContextOptionsBuilder<ApplicationDbContext>()
            .UseSqlite(_connection, null)
            .Options;

            ArrangeDbContext = new ApplicationDbContext(dbContextOptions);
            AssertDbContext = new ApplicationDbContext(dbContextOptions);

            ArrangeDbContext.Database.EnsureCreated();
            //AssertDbContext.Database.EnsureCreated(); not necessary because it will already be created from the line above.
        }

        [TearDown]
        public virtual void TearDown() {
            _connection.Close();
            _connection.Dispose();
            ArrangeDbContext.Dispose();
            AssertDbContext.Dispose();
        }

        public ApplicationDbContext ArrangeDbContext { get; set; }
        public ApplicationDbContext AssertDbContext { get; set; }
        public ApplicationDbContext ActDbContext => AssertDbContext;

        private SqliteConnection _connection;
    }
}