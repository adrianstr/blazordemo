﻿using BlazorDemo.Core.WeatherForecasts;
using BlazorDemo.Web.Pages;
using BlazorDemo.Web.Tests._Infrastructure;
using Bunit;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace BlazorDemo.Web.Tests.Pages {
    [TestFixture]
    public class FetchDataTests : ComponentTestBase {
        [Test]
        public void VisibleLoadingIndicatorOnStartup() {
            AutoResetEvent autoResetEvent = new AutoResetEvent(false);
            MediatorMock.Setup(m => m.Send(It.IsAny<WeatherForecastRequests.GetByDate>(), It.IsAny<CancellationToken>()))
                .Returns(Task.Run(() => {
                    autoResetEvent.WaitOne(1000);
                    return Array.Empty<WeatherForecast>();
                }));

            IRenderedComponent<FetchData> cut = TestContext.RenderComponent<FetchData>();
            string markup = cut.Markup;
            autoResetEvent.Set();

            markup.Should().Contain("<em>Loading...</em>");
        }


        [Test]
        public void ShowingWeatherforecastsOnceLoaded() {
            MediatorMock.Setup(m => m.Send(It.IsAny<WeatherForecastRequests.GetByDate>(), It.IsAny<CancellationToken>()))
                .Returns(Task.FromResult(Array.Empty<WeatherForecast>()));

            IRenderedComponent<FetchData> cut = TestContext.RenderComponent<FetchData>();

            cut.Markup.Should().NotContain("<em>Loading...</em>");
            cut.Markup.Should().Contain("id=\"weather-forecast-table\"");
        }
    }
}
