﻿using NUnit.Framework;
using Bunit;
using BlazorDemo.Web.Pages;
using BlazorDemo.Web.Tests._Infrastructure;

namespace BlazorDemo.Web.Tests.Pages {
    [TestFixture]
    public class CounterTests : ComponentTestBase {
        [Test]
        public void CounterShouldIncrementWhenSelected() {
            // Arrange
            var cut = TestContext.RenderComponent<Counter>();
            var paraElm = cut.Find("p");

            // Act
            cut.Find("button").Click();
            var paraElmText = paraElm.TextContent;

            // Assert
            paraElmText.MarkupMatches("Current count: 1");
        }
    }
}
