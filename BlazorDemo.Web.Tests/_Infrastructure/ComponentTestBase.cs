﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using NUnit.Framework;

namespace BlazorDemo.Web.Tests._Infrastructure {
    public class ComponentTestBase {
        [SetUp]
        public virtual void SetUp() {
            TestContext = new Bunit.TestContext();
            MediatorMock = new Mock<IMediator>();
            TestContext.Services.AddSingleton(MediatorMock.Object);
        }

        [TearDown]
        public virtual void TearDown() {
            TestContext.Dispose();
        }

        public Bunit.TestContext TestContext { get; private set; }
        public Mock<IMediator> MediatorMock { get; private set; }
    }
}
