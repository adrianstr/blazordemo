﻿using BlazorDemo.Core.WeatherForecasts;
using BlazorDemo.Database.EFCore;
using System;
using System.Linq;
using System.Collections.Generic;

namespace BlazorDemo.Database {
    public class WeatherForecastRepository : IWeatherForecastRepository {
        public WeatherForecastRepository(ApplicationDbContext dbContext) {
            DbContext = dbContext;
        }

        public ApplicationDbContext DbContext { get; set; }

        public WeatherForecast GetByBusinessId(Guid businessId) {
            throw new NotImplementedException();
        }

        public IList<WeatherForecast> GetByDate(DateTime day) {
            return DbContext.WeatherForecasts
                .Where(w => w.Date == day)
                .ToList();
        }

        public WeatherForecast GetById(int id) {
            throw new NotImplementedException();
        }
    }
}
