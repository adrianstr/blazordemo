﻿using BlazorDemo.Core.WeatherForecasts;
using BlazorDemo.Core.WeatherForecasts.ValueObjects;
using Microsoft.EntityFrameworkCore;
using System;

namespace BlazorDemo.Database.EFCore {
    public class ApplicationDbContext : DbContext {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) {
        }

        public DbSet<WeatherForecast> WeatherForecasts { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<WeatherForecast>().HasData(
                new WeatherForecast(new DateTime(2020, 3, 3), new Temperature(10, TemperatureUnit.Celcius), "definetly cold") { Id = 1 },
                new WeatherForecast(new DateTime(2020, 3, 4), new Temperature(10, TemperatureUnit.Celcius), "not so warm") { Id = 2 },
                new WeatherForecast(new DateTime(2020, 3, 4), new Temperature(20, TemperatureUnit.Celcius), "warm") { Id = 3 },
                new WeatherForecast(new DateTime(2020, 3, 5), new Temperature(20, TemperatureUnit.Celcius), "warm") { Id = 4 }
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}
