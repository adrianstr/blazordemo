﻿using System;

namespace BlazorDemo.Core._Infrastructure.Exceptions {
    [Serializable]
    public class UnsupportedEnumValueException<TEnum> : Exception where TEnum : Enum {
        public UnsupportedEnumValueException(TEnum unsupportedValue) : this(GetDefaultMessage(unsupportedValue)) { }
        public UnsupportedEnumValueException(TEnum unsupportedValue, Exception inner) : this(GetDefaultMessage(unsupportedValue), inner) { }
        public UnsupportedEnumValueException(string message) : base(message) { }
        public UnsupportedEnumValueException(string message, Exception inner) : base(message, inner) { }
        protected UnsupportedEnumValueException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }

        private static string GetDefaultMessage(TEnum unsupportedValue) {
            return $"Enum value is not supported. {{ Value: '{unsupportedValue}', Type: '{typeof(TEnum).FullName}' }}";
        }
    }
}
