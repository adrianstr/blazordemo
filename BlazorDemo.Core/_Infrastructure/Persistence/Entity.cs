﻿using System;

namespace BlazorDemo.Core._Infrastructure.Persistence {
    public class Entity {
        public Entity() {
            BusinessId = Guid.NewGuid();
        }

        public int Id { get; set;  }
        public Guid BusinessId { get; set; }

        public override bool Equals(object obj) {
            return obj is Entity entity &&
                   BusinessId.Equals(entity.BusinessId);
        }

        public override int GetHashCode() {
            return HashCode.Combine(BusinessId);
        }
    }
}