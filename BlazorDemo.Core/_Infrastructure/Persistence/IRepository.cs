﻿using System;

namespace BlazorDemo.Core._Infrastructure.Persistence {
    public interface IRepository<T> {
        T GetById(int id);
        T GetByBusinessId(Guid businessId);
    }
}
