﻿using BlazorDemo.Core._Infrastructure.Exceptions;

namespace BlazorDemo.Core.WeatherForecasts.ValueObjects {
    public struct Temperature {
        public Temperature(int value, TemperatureUnit unit) {
            Value = value;
            Unit = unit;
        }

        public int Value { get; set; }
        public TemperatureUnit Unit { get; set; }

        public int GetCelciusValue() {
            switch (Unit) {
                case TemperatureUnit.Celcius:
                    return Value;
                case TemperatureUnit.Fahrenheit:
                    return (Value - 32) * 5 / 9;
                default:
                    throw new UnsupportedEnumValueException<TemperatureUnit>(Unit);
            }
        }

        public int GetFahrenheitValue() {
            switch (Unit) {
                case TemperatureUnit.Celcius:
                    return Value * 9 / 5 + 32;
                case TemperatureUnit.Fahrenheit:
                    return Value;
                default:
                    throw new UnsupportedEnumValueException<TemperatureUnit>(Unit);
            }
        }

        public override bool Equals(object obj) {
            return obj is Temperature temperature &&
                   Value == temperature.Value &&
                   Unit == temperature.Unit;
        }

        public override int GetHashCode() {
            int hashCode = -177567199;
            hashCode = hashCode * -1521134295 + Value.GetHashCode();
            hashCode = hashCode * -1521134295 + Unit.GetHashCode();
            return hashCode;
        }
    }
}