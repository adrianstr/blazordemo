﻿namespace BlazorDemo.Core.WeatherForecasts.ValueObjects {
    public enum TemperatureUnit {
        Celcius = 1,
        Fahrenheit = 2
    }
}