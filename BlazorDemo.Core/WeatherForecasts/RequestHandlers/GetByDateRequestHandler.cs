﻿using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BlazorDemo.Core.WeatherForecasts.ActionHandler {
    public class GetByDateRequestHandler : IRequestHandler<WeatherForecastRequests.GetByDate, WeatherForecast[]> {

        public GetByDateRequestHandler(IWeatherForecastRepository repository) {
            Repository = repository;
        }

        public IWeatherForecastRepository Repository { get; set; }

        public Task<WeatherForecast[]> Handle(WeatherForecastRequests.GetByDate request, CancellationToken cancellationToken) {
            return Task.FromResult(Repository.GetByDate(request.Date).ToArray());
        }
    }
}
