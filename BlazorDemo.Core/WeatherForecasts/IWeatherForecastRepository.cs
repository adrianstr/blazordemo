﻿using BlazorDemo.Core._Infrastructure.Persistence;
using System;
using System.Collections.Generic;

namespace BlazorDemo.Core.WeatherForecasts {
    public interface IWeatherForecastRepository : IRepository<WeatherForecast> {
        IList<WeatherForecast> GetByDate(DateTime day);
    }
}
