﻿using MediatR;
using System;

namespace BlazorDemo.Core.WeatherForecasts {
    public static class WeatherForecastRequests {
        public record DeleteById(int id) : IRequest;
        public record GetByDate(DateTime Date) : IRequest<WeatherForecast[]>;
    }
}
