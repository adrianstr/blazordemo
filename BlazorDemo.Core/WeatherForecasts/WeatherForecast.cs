using BlazorDemo.Core._Infrastructure.Persistence;
using BlazorDemo.Core.WeatherForecasts.ValueObjects;
using System;

namespace BlazorDemo.Core.WeatherForecasts {
    public class WeatherForecast : Entity {
        [Obsolete("Nur f�r Reflection")]
        public WeatherForecast() {

        }

        public WeatherForecast(DateTime date, Temperature temperature, string summary) {
            Date = date;
            TemperatureC = temperature.GetCelciusValue();
            TemperatureF = temperature.GetFahrenheitValue();
            Summary = summary;
        }

        public DateTime Date { get; set; }
        public int TemperatureC { get; set; }
        public int TemperatureF { get; set; }
        public string Summary { get; set; }
    }
}
