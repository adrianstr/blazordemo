﻿using BlazorDemo.Core.WeatherForecasts.ValueObjects;
using FluentAssertions;
using NUnit.Framework;
using System;

namespace BlazorDemo.Core.WeatherForecasts.Tests {
    [TestFixture]
    public class WeatherForecastTests {
        [Test]
        public void Test_CreateWeatherForecast() {
            var date = new DateTime(2021, 2, 3);
            var temperature = new Temperature(20, TemperatureUnit.Celcius);
            string summary = "bewölkt aber flockig";
            var forecast = new WeatherForecast(date, temperature, summary);

            //then
            forecast.Summary.Should().Be(summary);
            forecast.TemperatureC.Should().Be(temperature.GetCelciusValue());
            forecast.TemperatureF.Should().Be(temperature.GetFahrenheitValue());
            forecast.Date.Should().Be(date);
        }
    }
}