﻿using BlazorDemo.Core.WeatherForecasts.ValueObjects;
using FluentAssertions;
using NUnit.Framework;

namespace BlazorDemo.Core.Tests.WeatherForecasts.ValueObjects {
    [TestFixture]
    public class TemperatureTests {
        [Test]
        public void Test_CanCreateTemperature() {
            var t = new Temperature(30, TemperatureUnit.Celcius);

            t.Value.Should().Be(30);
            t.Unit.Should().Be(TemperatureUnit.Celcius);
        }

        [Test]
        public void Test_GetCelciusValue_WhenUnitIsCelcius() {
            var t = new Temperature(30, TemperatureUnit.Celcius);

            t.GetCelciusValue().Should().Be(30);
        }

        [Test]
        public void Test_GetCelciusValue_WhenUnitIsFahrenheit() {
            var t = new Temperature(68, TemperatureUnit.Fahrenheit);

            t.GetCelciusValue().Should().Be(20);
        }


        [Test]
        public void Test_GetFahrenheitValue_WhenUnitIsFahrenheit() {
            var t = new Temperature(68, TemperatureUnit.Fahrenheit);
            t.GetFahrenheitValue().Should().Be(68);
        }

        [Test]
        public void Test_GetFahrenheitValue_WhenUnitIsCelcius() {
            var t = new Temperature(20, TemperatureUnit.Celcius);
            t.GetFahrenheitValue().Should().Be(68);
        }
    }
}