﻿using FluentAssertions;
using NUnit.Framework;
using System;

namespace BlazorDemo.Core._Infrastructure.Exceptions.Tests {
    [TestFixture]
    public class UnsupportedEnumValueExceptionTests {
        [Test]
        public void Test_CanCreateException() {
            var exception = new UnsupportedEnumValueException<TestEnum>(TestEnum.One);
        }

        [Test]
        public void Test_CtorWithDefaultMessage() {
            TestEnum unsupportedEnumValue = TestEnum.One;
            var exception = new UnsupportedEnumValueException<TestEnum>(TestEnum.One);

            AssertExceptionMessage(unsupportedEnumValue, exception);
            exception.InnerException.Should().BeNull();
        }

        [Test]
        public void Test_CtorWithDefaultMessageAndException() {
            var innerException = new Exception();
            TestEnum unsupportedEnumValue = TestEnum.One;
            var exception = new UnsupportedEnumValueException<TestEnum>(unsupportedEnumValue, innerException);

            AssertExceptionMessage(unsupportedEnumValue, exception);
            exception.InnerException.Should().Be(innerException);
        }

        [Test]
        public void Test_CtorWithCustomMessage() {
            var exception = new UnsupportedEnumValueException<TestEnum>("Custom message");

            exception.Message.Should().Be("Custom message");
            exception.InnerException.Should().BeNull();
        }

        [Test]
        public void Test_CtorWithCustomMessageAndInnerException() {
            var innerException = new Exception();
            var exception = new UnsupportedEnumValueException<TestEnum>("Custom message", innerException);

            exception.Message.Should().Be("Custom message");
            exception.InnerException.Should().Be(innerException);
        }

        private static void AssertExceptionMessage(TestEnum unsupportedEnumValue, UnsupportedEnumValueException<TestEnum> exception) {
            exception.Message.Should().Be($"Enum value is not supported. {{ Value: '{unsupportedEnumValue}', Type: '{typeof(TestEnum).FullName}' }}");
        }

        private enum TestEnum {
            One,
            Two
        }
    }
}